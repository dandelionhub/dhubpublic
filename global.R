###############################################################################+
# R Shiny App - Global                                                      ####
# The Dandelion Hub - Creating a movement of movements
#
# Author: Dandelion Team
# Date, Init: 2021-02-27
# Contact: dhub.global[AT]pm.me
#
###############################################################################+

source("./functions.R")

pkgs.required <- c("config", 
                   "shiny", "shinyjs", "shinythemes", "shinyWidgets", "DT", 
                   "rmarkdown", "knitr", "markdown", "kableExtra",
                   "leaflet",
                   "DBI",  "RMariaDB",
                   "httr", "jsonlite", "curl")

checkPackages(pkgs.required)

# Manual: system("chmod g+rw ./archive/")

##############################################################################+
# 1. set Frontend Configs (Web app) ####
##############################################################################+

# set App Description

app.title <- "The Dandelion Hub"
app.description <- "A hub for civil resistance actions for a better world."
app.demand <- "Seed action. Harvest change."
app.version <- "1.90.00"

# set R Options

options(digits=22) # increase precisions of Longitude, Latitude for write.table()

# set Variable List

ddb.vars <- c(
 "ActionSeriesId", "ActionInstanceId", "ActionInstanceVersion", "LatestVersion",
 "ActionTitle", "ActionDescription", "ActionDemand", "ActionTagsIssue",
 "Affiliations", "Activists", "ActivistsSource", "ActivistsSourceLink", "MediaLink", "InfoLink", 
 "ActionStatus", "ActionDate", "TimeStart", "TimeStop", 
 "LocationSource", "LocationType", "LocationContinent", "LocationCountry",  "LocationRegion",
 "LocationCity", "LocationPostcode", "LocationStreet", "LocationStreetNr",
 "LocationWaterbody",
 "LocationLatitude", "LocationLongitude",
 "TypeOfAction", "ActionLevels", 
 "GeneSharpLevelA", "GeneSharpLevelB", "GeneSharpLevelC", 
 "TimeStampUTC", "AppVersion")

datasheet.vars <- ddb.vars

datasheet.vars.simple <- c(  
  "ActionTitle", 
  "Affiliations", "Activists", "InfoLink", 
  "ActionDate",  "TimeStart", "TimeStop", 
  "LocationContinent", "LocationCountry",
  "LocationCity", "LocationStreet", "LocationStreetNr",
  "LocationLatitude", "LocationLongitude",
  "ActionSeriesId", "ActionInstanceId", "ActionInstanceVersion", "LatestVersion")

datasheet.vars.advanced <- datasheet.vars 

# set HTML tags

div.style <- "width:75%;margin-left:auto;margin-right:auto;"
width.default <- "100%"

hr1.style <- paste0("width:", width.default, 
                   ";text-align:left;margin-left:0;", 
                   "background-color: #ff8000; height: 9px; border: 0")

hr2.style <- paste0("width:", width.default, 
                    ";text-align:left;margin-left:0;", 
                    "background-color: #ff8000; height: 3px; border: 0")

dropdown.list.length.max <- 500

# set Map configs ####

start.map.static.filter <- "ActionDate >= DATE_SUB(CURRENT_DATE(), INTERVAL 30 DAY)"
start.map.static.greta.filter <- "ActionSeriesId = '1' AND 
                                  ActionDate >= '2018-08-20' AND 
                                  ActionStatus = 'Performed'"

dynamic.map.days.before.today.min     <- -30
dynamic.map.days.before.today.default <- -30

# set Social Media

mastodon.url <- "https://dhub.social"

twitter.text <- paste0(
 "https://twitter.com/intent/tweet?",
 "text=The%20Dandelion%20Hub%20is%20creating%20a%20movement%20of%20movements%20",
 "for%20science,%20people%20power%20and%20a%20livable%20future!%20Join%20now!",
 "&url=https://dhub.global")

##############################################################################+
# 2. set Backend Configs (Database) ####
# -production private: production database with full access info (rws)
# -production public: production database with limited access info (read only)
# -development private: development database with full acces info (rws)
##############################################################################+

config.file <- c("config_pro_private.yml", 
                 "config_pro_public.yml",
                 "config_dev_private.yml")[3]

if(grepl(pattern="_pro_", x=config.file)) {
  app.version.db <- paste0(app.version,"P")
} else if( grepl(pattern="_dev_", x=config.file) ){
  app.version.db <- paste0(app.version,"D")
}

initDB(sim.number.aids = 0, sim.number.versions = 0, 
       add.gretathunberg.tf = TRUE)

##EOF###########################################################################+

