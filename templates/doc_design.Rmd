---
title: "DHub Design"
author: "The Dandelion Team"
date: "2021-02-27"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
setwd("../") # only for this code chunk
source("global.R")
checkPackages(pkgs.required)
```

```{css, echo=FALSE}
caption {
  color: black;
  font-weight: bold;
  font-size: 1.5em;
}

th, td { padding: 6px; }
```

<style>
blue { color: #007fff }
orange { color: #ff8000  }
</style>

# <blue>1. Background</blue> 

The **dandelion** (from  *dent de lion* (French), *tooth of a lion*, English) describes
a simple, common flower (ie lat. *Taraxacum*), which can spread its seeds by the wind
across large distances. The tooth of a lion is a dangerous weapon.

The name aims to reflect the **dualism of civil resistance**, which uses
non-violent action to confront injustice, as its leading principle. 
Civil resistance and should neither be misunderstood as fainthearted passivity 
nor as destructive violence to keep its strength and direction. Thereby, actions
of common people can turn into mighty weapons. 

Metaphorically speaking, **a society is like a human body**, whose bones represent 
the political and economic system and whose muscles represent its people. If the 
human body moves towards a dangerous cliff, the muscles should slowly stop working. 
The body may then move forward, lose its strength and fall to the ground (or even
fall over the cliff). Alternatively, the body may change direction towards safety
and gain strength again.

Likewise if **people passively or actively resist** to support and collaborate with 
a dangerous political and economic system, it will also face the decision to continue,
risk losing power and wealth and finally end up in a crisis - or change direction 
and gain support by the people again. The art is to weaken the body 
(or a society) just enough to make it change direction while avoiding
unnecessary harm (e.g. violent conflicts) and strengthen and coordinate the body 
again when it is moving in the right direction.

# <blue> 2. The Challenge</blue> 

Progressive movements have several **weaknesses** since they are usually
organized bottom-up ("grassroots"). This means they are usually initiated by individuals,
decentralized, diverse, with flat hierarchies, support a great variety of ideas 
with varying degrees of consensus, and, finally, run on a minimal budget. These factors
limit the efficacy of movements because it requires resources in terms
of time and energy to reach a consensus, consensus on goals and methods is often minimal,
and finally may compromise its implementation because of an ineffective organizational 
structure and insufficient funding. Finally, the egalitarian structure may make
it harder for individuals to step up to become discussion leaders or action leaders.

# <blue> 3. The Opportunity</blue> 

However, progressive movements have also multiple **strengths**. They usually represent 
moral justice and common sense, can mobilize large crowds, infiltrate a 
great variety of organisations and institutions of the political and economic system, 
and can communicate, organize, and share resources effectively in a trusted and 
mutually loyal network. In addition, the egalitarian structure may also make it easier
for motivated individuals to take on leadership roles if they receive sufficient
support from the rest of the group to enable them to act effectively as leaders.

**The Dandelion Hub** aims at the individual, decentralized, unorganized, and diverse nature 
of progressive movements by providing **a collaborative, centralized, organized, and
united platform** for progressive movements and their actions. It covers 
the full range of civil resistance and allows sharing information about actions 
between activists and the rest of society. Furthermore, activists can socialize
with each other and organize actions together via Dandelion Network. Thereby, the 
Dandelion Hub creates synergies and triggers social dynamics.

For **example**, if 100 individuals occasionally plant trees without knowing about 
each other or speaking about it, it is just 100 individuals planting trees. 
However, if 100 individuals plant trees because of a shared vision to protect the
climate and biodiversity on the same day and tell others about it, they are
a movement, which can claim success (eg report on a successful "tree-planting day" 
in the mass media), change social norms (eg "Our neighbours 
have planted trees, maybe we should too?") and trigger social changes (eg 
planting trees is becoming a trend). The Dandelion Hub will then allow to register
each activity ("Enter Action"), report them ("Show Datasheet", "Show Table"),
summarize them ("Show Summary") and display the number of activists over space
("Show Map (Static)") and time ("Show Map (dynamic)").

The **greater visibility** of such protests will make it harder for political and
economic leaders to claim morality or common sense. Thereby, movements can create
pressure and actively influence policy decisions because of the fear of decision
makers that the people will realize "that the emperor has no clothes".

In addition, following the principle of **"see one, do one, teach one"** progressive
movements can provide information about them and their actions, which may
inspire others and motivate them to replicate, modify, or improve actions.

# <blue> 4. The Solution</blue> 

## <blue> 4.1 Design Principles</blue> 

The Dandelion Hub aims to provide a global, minimalist, robust, open-source, 
open-access, transparent, fully documented and cost-effective hub 
(ie front-end web app with a back-end database and an associated social network)
to plan, organize, record, and report decentral actions for science, people power 
and a livable future from all over the world.

## <blue> 4.2 Web App - Frontend</blue> 

The Dandelion Hub provides a simple web user interface for activists world-wide with following functions:

* **Enter Action**: register or update an action
* **Show Actions**
  + **Show Datasheet**: show a single previous action
  + **Show Table**: show multiple previous actions
  + **Show Summary**: show a summary of multiple previous actions
  + **Show Map (Static)**: show a static ("frozen") map of multiple previous actions
  + **Show Map (Dynamic)**: show a dynamic ("movie") map of multiple previous actions over time
* **Documentation**
  + **Global Crises**: learn about human-caused global crises
  + **Civil Resistance**: learn about 198 methods of civil resistance (Sharp, 1973)
  + **DHub Tutorials**: learn about using the Dandelion Hub
  + **DHub Design**: learn about the design principles of The Dandelion Hub
  + **DHub Promotion**: learn about promoting The Dandelion Hub
  + **DHub Version**: learn about the current version of The Dandelion Hub
  + **Disclaimer**:  learn about limitations of The Dandelion Hub's responsibility
  + **Contact**: learn how to contact The Dandelion Hub

## <blue> 4.3 Database - Backend</blue> 

The collected data contains are grouped into three categories:

* **Required Info**: This is the minimal information which is required for analysis.
* **Additional Info**: This is additional information which is useful for analysis.
* **Expert Info**: This information allows one to amend previous action information and 
link multiple actions in a series.

In summary, information in the following variables is collected (see table below).
All information in the database is publicly available. Therefore, no sensitive
data should be shared here.

---

```{r results="asis", echo=FALSE}
ddb.schema.df <- getDDBSchema()[,-1]
ddb.schema.df %>%
  kable(caption = "Table 1: Schema of the The Dandelion Hub database", format= "html") %>%
  kable_styling(bootstrap_options = "bordered", full_width = TRUE)
```

---

## <blue> 4.4 Actions: Precision, Bias, Representativeness</blue> 

Reported actions from diverse sources may be imprecise, inaccurate (biased), 
or not representative. However, by collecting verifiable information on a broad range
of actions from multiple sources, which can be updated as needed, while keeping a fully
documented track of all records, we expect to obtain valid data (cf Wikipedia approach).

For example, while the counting of activists in smaller actions is expected to be reliable,
the estimation of (moving) large crowds of people is expected to be more unreliable with 
a greater risk of being biased by interests of various parties. However, by combination 
of different sources (e.g. activists, journalists,  police) and using a methodological 
approach (see [crowd counting] (https://en.wikipedia.org/wiki/Crowd_counting), 
esp. Jacobs' Method) with support from fotos and videos (as feasible) error and bias are
expected to be minimized. 

However, the number of protest actions or participating activists,
respectively, is just one indicator of the public opinion relevant in a democratic
system. Since votes may be based on partial information, personal agendas,
which may change over time -  although of great value - should not be used to 
invalidate universal basic principles such as the preservation of the biosphere
for future generations in the same way as a popular vote should not determine 
the outcome of a mathematical equation.

## 4.5 Outcomes: Precision, Bias, Representativeness [Planned]

To allow assessing the efficacy and safety of actions, The Dandelion Hub is also 
planning to collect information on various outcomes. For example, the impact of
actions could be measured by:

* Benefits:
  + Size of audience (eg viewers, Listeners, Followers)
  + Number of reports, posts, likes on social media (Facebook, X/Twitter, Instagramm)
  + Number of reports in mass media (eg newspapers, radio, TV)
  + Number of new joiners
  + Amount of raised funding
  + Number of present police officers or security guards
  + Number of present cars of police or security
  + Reduction of carbon emissions in tons CO2e
  + Change of any of the outcomes above relative to last action (as reference)
* Costs:
  + Political and economic costs for society
  + Costs for activists (time, energy, money, and other resources)
  + Fines for activists
  + Number of activists on trial
  + Number of activists in prison
  + Number of activists leaving movement
  + Amount of lost funding
* Benefit-Cost Balance

However, the indirect and direct impact of civil resistance actions
is often hard to measure because actions are diverse, aim at multiple directions
at the same time and show delayed effects which are often hard to observe at 
an action or in the immediate time after. Especially, the effect of actions on
social media are hard to track. Therefore, (so far) no measures of impact are recorded.
However, a description of actions and their impact would be desirable from a 
research perspective to evaluate the efficacy of various types of actions.

## <blue> 4.6 The Dandelion Network</blue> 

The Dandelion Hub provides a unique opportunity to bring like-minded
people from all over world together. To catalyse networking between these people
a free, open-soure, federated social network based on the Mastodon Software (using 
the ActivityPub protocol) was integrated into the Dandelion Hub.

The Dandelion Network is focusing on a growth of a network people in a trusted,
open environment and provides only medium protection of data and communication.
Alternative technical platforms may be used depending on the needs of the 
activists, e.g., for higher data protection standards.

# <blue> 5. Safety and Privacy</blue> 

## <blue>5.1 General</blue> 

Civil resistance actions can elicit strong feelings by individual citizens, in the media,
politics, business, and the executive, legal, and judicial system. Activists move 
on a fine line between maximizing their impact on society and minimizing risks for themselves 
and others. While a certain level of risk is not avoidable, each activist should
be aware of the context, type and potential consequences of an action. Therefore,
each activist has the responsibility to protect himself/herself and other activists,
for example, by controlling the information about himself/herself and others to 
avoid unwanted consequences (cf stalking, mobbing, doxing, consequences from 
colleagues or employer, legal consequences, etc). Consideration should also be given
to the fact that in a global movement activists act in different political systems and 
societal contexts, e.g. regarding their civil rights or levels of discrimination.

Therefore, depending on the action sensitive information, eg names, addresses,
fotos, should be handled with care and as feasible in agreement with the 
concerned individuals. The use of single-purpose or single-use email addresses or
phone numbers ("burner email", see [ProtonMail](https://protonmail.com),
"burner phone/SIM card") and anonymous websites (see [PasteBin](https://www.pastebin.com))
maybe recommended. 

Electronic devices during actions pose a potential safety risk, if the stored
information is accessed by unauthorized individuals, institutions or 
organizations. While it maybe safest, not to bring them to the action, it will often
be needed to communicate and work effectively. Recommendations on secure usage and
communication have been published elsewhere [[Web](https://xrbristol.org.uk/secure-communications/)].

## <blue> 5.2 Safety, Privacy and The Dandelion Hub</blue> 

**"The Dandelion Hub" makes all reported data publicly available in an open-access database.**
Therefore, sensitive information which may put the reporting activist or others
at risk, should only be reported if the activist and other affected activists are
aware of the risks and accept them. The reporting individual is primarily
responsible for protecting himself and others ("As private as you make it!") and,
therefore, the Dandelion Team does not take any responsibility for the information
which was contributed by activists.

Data which was retrieved with a filter expression, e.g. "ActionSeriesId=1" can be
downloaded as text (.csv) file where the files are automagically named with a filter 
identifier. The **filter identifier and the filter expression is stored in the database**,
to be able to explain the contents of a file by using its filename. At present the
contents of the filter database (table "ddb_filter") is not availabe in the web app,
but can be accessed via the database interface via a SQL program or client.

If sensitive information is identified which should be removed, it should be reported to the 
[Dandelion Team](mailto:dhub.global@pm.me) who will assess each report and if so take 
measures after a benefit-risk assessment in an appropriate time frame considering 
that this a minimal-budget, non-profit website.

# <blue> 6. Tech Details</blue> 

The website is using the **RStudio** development platform for *R* (version 4.2.1) 
and the **Shiny** web development framework. The web app frontend (webserver NGINX) and 
the database backend (database MariaDB) backend is hosted by the web provider *hosting.de* 
(Germany), who is operating with 100% renewable energy.

If the user decides to "Guess location from Internet address (IP)", only then the
[dhub.global](https://dhub.global) web app will contact the 
internet providers [ipify.org](https://ipify.org) to retrieve the user's public IP address
and [ipstack.com](https://ipstack.com) to geolocate the IP address. Ipify.org and
ipstack.com both use industry-level encryption and do not store IP or person-related information
from such API requests. Web analytics are collected via [plausible.io](https://www.plausible.io),
a simple, light-weight, privacy-focused, open-source web service 
(see [Web Statistics](https://plausible.io/dhub.global)).

The geolocation can also be retrieved by 'half-manually' selecting the appropriate 
geographic levels including the city for which the geometric coordinates (Latitude,
longitude) of the geometric centre (centroid) are then calculated. The same database
as for the "fully automatic" GeoIP location is used for "half-automatic geolocation"
to enable  consistent names for data. The used database is the Maxmind GeoLite 2 dataset
for geographic on-shore locations, and the ESRI World Water Body dataset
for off-shore locations.

This web application separates the web frontend and the database backend, so 
in principle additional web apps (eg per country, per language) or websites
with different software stacks could be used, as long as they connect to the same 
database.

All data in the Dandelion Database can be retrieved via a programming interface
using an SQL client (cf [https://dbeaver.io](https://dbeaver.io)). The access-information
for the database to query the data (read only!) is stated below:

```{r echo=FALSE}
config.readonly <- data.frame(t(data.frame(config::get(config="mariadbremoteread", file="config_pro_public.yml"))))
names(config.readonly) <- "VALUE"
config.readonly <- config.readonly[rownames(config.readonly) != "ipstackkey",, drop = FALSE]
print(config.readonly)
```

# <blue>References</blue> 

Sharp, Gene (1973). The Politics of Nonviolent Action  (Part 2). Boston: Porter Sargent Publishers.
[WEB](https://www.aeinstein.org/books/the-politics-of-nonviolent-action-part-2/)

Engler, M. & Engler, P. (2019). This is an uprising! Avalon Publishing Group, http://thisisanuprising.org. 

