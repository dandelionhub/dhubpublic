---
title: "DHub Tutorials - Enter Action"
author: "The Dandelion Team"
date: "2021-02-27"
output: html_document
---

<style>
blue { color: #007fff }
orange { color: #ff8000  }
</style>

### <blue>2.3 The Dandelion Hub - Show Actions</blue>

Hallo, this is a short tutorial on how to show the details of actions into the Dandelion Database.

If you click on the "Show Actions" tab, you can see various sub-panels:

* Go to **"Show Datasheet"** if you want to see the data of a single version of an action instance by entering the ActionInstanceId and the version number in the web form and press "SHOW ACTION DATASHEET!". You can also "DOWNLOAD THE ACTION DATASHEET!" as a text file to save if locally.

* Go to **"Show Table"** if you want to see the date of multiple action instances by entering a filter text like those given in the examples and press "FILTER DATA & UPDATE TABLE!". You can even quick-sort and quick-filter the resulting table.

* Go to **"Show Summary"** if you want to summarize multiple actions instances in terms of number of actions, number of activists, number and list of cities, number and list of countries and so on. This summary of actions may be very useful for organizers and journalists to summarize multiple actions. Again, you can use a filter test like those given in the examples and press "FILTER DATA & UPDATE TABLE!".

* Go to **"Show Map (Static)"** if you want to show the geographic location of a set of actions on a static, this means "frozen", map.  Again, you can use a filter like those given in the examples and press "FILTER DATA & UPDATE MAP!".

* Go to **"Show Map (Dynamic)"** if you want to show the geographic location of a set of actions dynamically, this means over time, in an animated movie. You can use the calender menus and the slider bar to quickly adjust the range of dates and press the "Play" and "Pause" links to start and stop the movie. Again, you can use a filter like those given in the examples and press "FILTER DATA & UPDATE MAP!".

Thank you for your interest in the Dandelion Hub! Take care! :-)





