# README #

**The Dandelion Hub** (short: *DHub*) is a *central platform* to plan, organize, record, and report 
*decentral actions* for science, people power and a livable future from all over the world.

It features the **Dandelion Database** to store past, present, and future actions and the **Dandelion Networks** 
(based on the MASTODON and MATTERMOST software) to socialize with other like-minded people, organize and advertise actions.

The **Dandelion Team** believes that aggregating single actions of different movements and 
presenting them as **one collaborative movement for a better world** increases its impact on society.

### What is this repository for? ###

This is the code repository of "The Dandelion Hub", which includes a web app frontend 
and a database backend.

### Disclaimer ###

This program is free software. You can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software Foundation;

This program is distributed in the hope that it will be useful, but
without any warranty, without even the implied warranty of
merchantability or fitness for a particular purpose.  See the GNU
General PublicLicense for more details. You can retrieve a copy of the
GNU public licence version 3 online (https://www.gnu.org/licenses/gpl-3.0.en.html).

### Who do I talk to? ###

* Contact: dhub.global[at]pm.me